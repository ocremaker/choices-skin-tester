/** 
 * AO3 Choices skin v2.0 tester
 * Copyright (C) 2023  ocremaker
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import parseHTML from './parser.js'

let maxChars = 500_000

function updateCharsCount(charsCount) {
    let chracterCount = document.querySelector('#character-count')
    let color = 'red'
    if(charsCount < (maxChars*3/5))
        color = 'black'
    else if(charsCount < (maxChars*4/5))
        color = 'gold'
    else if(charsCount < maxChars)
        color = 'orange'
    else if(charsCount < maxChars)
        color = 'orange'
    chracterCount.innerHTML = `Characters count: <span style="color: ${color}">${charsCount.toLocaleString()}</span> (max ${maxChars.toLocaleString()}).`
}

window.addEventListener('load', () => {
    let frame = document.querySelector('#workskin')
    document.querySelector('#input').addEventListener('input', (e) => {
        let html = parseHTML(e.target.value)
        frame.innerHTML = html
        updateCharsCount(html.length)
    })
})
