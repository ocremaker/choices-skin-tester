/** 
 * AO3 Choices skin v2.0 tester
 * Copyright (C) 2023  ocremaker
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import error from './error.js'

// Authorized elements & attributes from AO3.
const AUTHORIZED_ATTRIBUTES = [
    "align", "alt", "axis",
    "class", "height", "href",
    "name", "src", "title",
    "width"
]

const AUTHORIZED_ELEMENTS = [
    "a", "abbr", "acronym", "address",
    "b", "big", "blockquote", "br",
    "caption", "center", "cite",
    "code", "col", "colgroup",
    "dd", "del", "details", "dfn", "div", "dl", "dt",
    "em", "figcaption", "figure",
    "h1","h2","h3","h4","h5","h6","hr",
    "i", "img", "ins", "kbd", "li",
    "ol", "p", "pre", "q", "s",
    "samp", "small", "span", "strike",
    "sub", "summary", "sup", "table", "tbody", "thead",
    "tfoot", "td", "th", "tr", "tt",
    "u", "ul", "var"
]

const PARSER = new DOMParser();

/**
 * Checks if a given attribute is authorized and returns it if true, returns "" otherwise.
 * 
 * @param {string} fullAttribute - Full text of the attribute.
 * @param {string} attributeName - Name of the attribute.
 * @return fullAttribute if authorized, "" otherwise.
 */
function _replaceAttribute(fullAttribute, attributeName) {
    if(AUTHORIZED_ATTRIBUTES.includes(attributeName))
        return fullAttribute
    return ""
}

/**
 * Strips all unauthorized attributes from a text.
 * 
 * @param {string} text - Full HTML.
 * @return Stripped HTML text.
 */
function _stripAttributes(text) {
    return text.replace(/ ([\w_-]+)="[^"]*"/g, _replaceAttribute).replace(/ ([\w_-]+)='[^']*'/g, _replaceAttribute)
}

/**
 * Checks if a given element is authorized and returns it if true, replaces its < by &lt; otherwise.
 * 
 * @param {string} fullElement - Full text of the element.
 * @param {string} elementName - Name of the element.
 * @return fullAttribute if authorized, replaces its < by &lt; otherwise.
 */
function _replaceUnauthorizedElement(fullElement, elementName) {
    if(AUTHORIZED_ELEMENTS.includes(elementName))
        return fullElement
    error(`Warning! The text contains the element ${elementName}, which is unauthorized on AO3.\nIt will be replaced as text only in the output.`)
    return fullElement.replace('<', '&lt;')
}

/**
 * Strips all unauthorized elements from a text.
 * 
 * @param {string} text - Full HTML.
 * @return Stripped HTML text.
 */
function _stripUnauthorizedElements(text) {
    return text.replace(/<([\w_-]+)( |>)/g, _replaceUnauthorizedElement).replace(/<\/([\w_-]+)>/g, _replaceUnauthorizedElement)
}

/**
 * Adds HTML line breaks for each line break.
 * 
 * @param {string} text - Full HTML.
 * @return Stripped HTML text.
 */
function _addLineBreaks(text) {
    return text.replace(/([^>]|\n)\n/gm, "$1<br/>\n")
}

/**
 * Sets named anchors IDs to match their name for newer browser support.
 * AO3 does the same in the background.
 * 
 * @param {string} text - Full HTML.
 * @return Stripped HTML text.
 */
function _changeNamedAnchordToLinks(text) {
    return text.replace(/<a name="([\w\d -]+)"/g, "<a id='$1' name='$1'")
}
    
/**
 * Parses HTML for AO3 compatibility.
 * 
 * @param {string} text - Full HTML.
 * @return Stripped HTML text.
 */
export default function(text) {
    error('') // Reset error.
    let output = PARSER.parseFromString(text, "text/html")
    let errorNode = output.querySelector("parsererror")
    if(errorNode) {
        error(`Invalid HTML provided: ${errorNode.innerText}.`)
        return ""
    }
    return _changeNamedAnchordToLinks(_addLineBreaks(_stripUnauthorizedElements(_stripAttributes(text))))
}
