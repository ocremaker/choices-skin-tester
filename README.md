# AO3 Choices Skin v2 tester

---

This repository contains the source code for the AO3 skin tester website, where you can paste HTML to test it with the Choices skin on.

## Legal

This repository, with the exception of the `skins` repository is licensed under GPLv3 (see `LICENSE.md`).

The `skins` repository is licensed under MIT (See `skins/LICENSE`)
